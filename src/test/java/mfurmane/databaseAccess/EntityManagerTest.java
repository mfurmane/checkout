package mfurmane.databaseAccess;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;
import java.util.Vector;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EntityManagerTest {

	EntityManager manager = new EntityManager();
	private int entitiesAdded = 0;
	private int connectionsAdded = 0;

	@Before
	public void before() {
		manager.setDao(new EntityDAOInterface() {

			@Override
			public Entity insertEntity(String id, String name, double price, int extraPriceCount, double extraPrice) {
				entitiesAdded += 1;
				return null;
			}

			@Override
			public Entity getEntityByIdentifier(String id) {
				Entity entity = new Entity();
				entity.setIdentifier(id);
				return entity;
			}
		}, new CheaperConnectionDAOInterface() {

			@Override
			public CheaperConnection insertDiscount(String firstIdentifier, String secondIdentifier, double discount) {
				connectionsAdded += 1;
				return null;
			}

			@Override
			public List<CheaperConnection> getCheaperConnections() {
				List<CheaperConnection> cheaperConnections = new Vector<>();
				CheaperConnection connection = new CheaperConnection();
				cheaperConnections.add(connection);
				return cheaperConnections;
			}
		});
	}

	@Test
	public void prepareDataTest() {
		Assert.assertEquals(0, entitiesAdded);
		Assert.assertEquals(0, connectionsAdded);
		manager.prepareData();
		Assert.assertEquals(6, entitiesAdded);
		Assert.assertEquals(2, connectionsAdded);
	}

	@Test
	public void getEntityTest() {
		Entity entity = manager.getEntityByIdentifier("AAAA");
		assertEquals("AAAA", entity.getIdentifier());
	}

	@Test
	public void getCheaperConnectionsTest() {
		List<CheaperConnection> connections = manager.getCheaperConnections();
		assertNotEquals(null, connections);
		assertEquals(1, connections.size());
	}

}
