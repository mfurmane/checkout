package mfurmane.databaseAccess;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheaperConnectionDAOTest {

	private CheaperConnectionDAO dao = new CheaperConnectionDAO();
	private SessionFactory sessionFactory;
	
	@Before
	public void before() {
		MetadataSources metadataSources = new MetadataSources();
		metadataSources.addFile("src/main/resources/hibernate.cfg.xml");
		Configuration configuration = new Configuration();
		sessionFactory = configuration.configure().buildSessionFactory();
		dao.setSessionFactory(sessionFactory);
	}

	@Test
	public void getCheaperConnectionsTest() {
		Assert.assertNotEquals(null, dao.getCheaperConnections());
	}
	
	@Test
	public void insertDiscountTest() {
		// main test
		int size = dao.getCheaperConnections().size();
		dao.insertDiscount("CCCC", "BBBB", 10);
		Assert.assertEquals(size + 1, dao.getCheaperConnections().size());

		// cleaning
		Connection id = new Connection();
		id.setFirstId(new Entity());
		id.getFirstId().setIdentifier("CCCC");
		id.setSecondId(new Entity());
		id.getSecondId().setIdentifier("BBBB");
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		Query<CheaperConnection> query = session.createQuery("delete CheaperConnection where connectionId = :id");
		query.setParameter("id", id);
		query.executeUpdate();
		Assert.assertEquals(size, dao.getCheaperConnections().size());
		session.close();
	}

}
