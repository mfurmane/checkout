package mfurmane.databaseAccess;

import static org.junit.Assert.fail;

import javax.persistence.PersistenceException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EntityDAOTest {

	private EntityDAO dao = new EntityDAO();
	private SessionFactory sessionFactory;

	@Before
	public void before() {
		MetadataSources metadataSources = new MetadataSources();
		metadataSources.addFile("src/main/resources/hibernate.cfg.xml");
		Configuration configuration = new Configuration();
		sessionFactory = configuration.configure().buildSessionFactory();
		dao.setSessionFactory(sessionFactory);
	}

	@Test
	public void getEntityByIdentifierTest() {
		Entity entity = dao.getEntityByIdentifier("AAAA");
		if (entity == null) {
			dao.insertEntity("AAAA", "A", 40, 3, 70);
			entity = dao.getEntityByIdentifier("AAAA");
		}
		Assert.assertNotEquals(null, entity);
		Assert.assertEquals("AAAA", entity.getIdentifier());
	}

	private void cleanEntity(String id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		Query<Entity> query = session.createQuery("delete Entity where identifier = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}

	@Test
	public void insertEntityTest() {
		if (dao.getEntityByIdentifier("UNUS") != null)
			fail("Test id (UNUS) is used, please delete it, or change test");
		dao.insertEntity("UNUS", "Unused", 997, 112, 1410);
		Assert.assertEquals("UNUS", dao.getEntityByIdentifier("UNUS").getIdentifier());
		cleanEntity("UNUS");
		Assert.assertEquals(null, dao.getEntityByIdentifier("UNUS"));
	}
	
	@Test(expected = PersistenceException.class)
	public void insertTooBigIdTest() {
		dao.insertEntity("UNUSE", "Unused2", 997, 112, 1410);
	}
	
	

}
