package mfurmane.mainEngine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import mfurmane.databaseAccess.Entity;
import mfurmane.nearUserObjects.BasketInterface;
import mfurmane.nearUserObjects.Receipt;

public class CheckoutControllerTest {

	CheckoutController controller = new CheckoutController(new BasketInterface() {

		@Override
		public void prepareData() {
			dataPrepared = true;
		}

		@Override
		public void clear() {
			basketCleaned = true;
		}

		@Override
		public Entity addItems(String id, long count) {
			items.add(id);
			if (id != "EVIL")
				return new Entity();
			else
				return null;
		}

		@Override
		public Receipt actualReceipt() {
			return new Receipt();
		}
	});

	private boolean dataPrepared = false, basketCleaned = false;
	private Set<String> items = new TreeSet<>();

	@Test
	public void addToBasketTest() {
		assertEquals(0, items.size());
		assertNotEquals(null, controller.addToBasket("AAAA", 1));
		assertEquals(1, items.size());
	}
	
	@Test(expected = RuntimeException.class)
	public void wrongAddToBasketTest() {
		controller.addToBasket("EVIL", 1);
	}

	@Test
	public void getActualPriceTest() {
		assertNotEquals(null, controller.getActualPrice());
	}

	@Test
	public void clearBasketTest() {
		assertEquals(false, basketCleaned);
		controller.clearBasket();
		assertEquals(true, basketCleaned);
	}

	@Test
	public void prepareDataTest() {
		assertEquals(false, dataPrepared);
		controller.prepareData();
		assertEquals(true, dataPrepared);
	}

}
