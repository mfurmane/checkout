package mfurmane.mainEngine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import mfurmane.databaseAccess.CheaperConnectionDAOTest;
import mfurmane.databaseAccess.EntityDAOTest;
import mfurmane.databaseAccess.EntityManagerTest;
import mfurmane.nearUserObjects.BasketTest;

@RunWith(Suite.class)
@SuiteClasses({ BasketTest.class, CheaperConnectionDAOTest.class, CheckoutControllerTest.class, EntityDAOTest.class, EntityManagerTest.class})
public class AllTests {

}
