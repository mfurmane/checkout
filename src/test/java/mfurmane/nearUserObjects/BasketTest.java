package mfurmane.nearUserObjects;

import java.util.List;
import java.util.Vector;

import org.junit.Assert;
import org.junit.Test;

import mfurmane.databaseAccess.CheaperConnection;
import mfurmane.databaseAccess.Connection;
import mfurmane.databaseAccess.Entity;
import mfurmane.databaseAccess.EntityManagerInterface;

public class BasketTest {

	private boolean dataPrepared = false;

	BasketInterface basket = new Basket(new EntityManagerInterface() {

		@Override
		public void prepareData() {
			dataPrepared = true;
		}

		@Override
		public Entity getEntityByIdentifier(String identifier) {
			return new Entity(identifier, identifier, 20, 2, 30);
		}

		@Override
		public List<CheaperConnection> getCheaperConnections() {
			List<CheaperConnection> cheaperConnections = new Vector<>();
			CheaperConnection cheaperConnection = new CheaperConnection();
			cheaperConnection.setConnectionId(new Connection());
			cheaperConnection.getConnectionId().setFirstId(new Entity("AAAA", "AAAA", 20, 2, 30));
			cheaperConnection.getConnectionId().setSecondId(new Entity("DDDD", "DDDD", 20, 2, 30));
			cheaperConnection.setSave(10);
			cheaperConnections.add(cheaperConnection);
			return cheaperConnections;
		}
	});

	@Test
	public void testPreparing() {
		Assert.assertFalse(dataPrepared);
		basket.prepareData();
		Assert.assertTrue(dataPrepared);
	}

	@Test
	public void testAdding() {
		Assert.assertEquals(0, basket.actualReceipt().getTotalPrice(), 0);
		Assert.assertEquals(0, basket.actualReceipt().getDiscount(), 0);
		basket.addItems("AAAA", 1);
		Assert.assertEquals(20, basket.actualReceipt().getTotalPrice(), 0);
		Assert.assertEquals(0, basket.actualReceipt().getDiscount(), 0);
	}

	@Test
	public void testMultipricing() {
		basket.addItems("AAAA", 3);
		Assert.assertEquals(50, basket.actualReceipt().getTotalPrice(), 0);
	}

	@Test
	public void testCheaperConnections() {
		basket.addItems("AAAA", 2);
		basket.addItems("DDDD", 3);
		Assert.assertEquals(60, basket.actualReceipt().getTotalPrice(), 0);
		Assert.assertEquals(20, basket.actualReceipt().getDiscount(), 0);
	}

	@Test
	public void testClearing() {
		basket.addItems("AAAA", 2);
		basket.clear();
		Assert.assertEquals(0, basket.actualReceipt().getTotalPrice(), 0);
		Assert.assertEquals(0, basket.actualReceipt().getDiscount(), 0);
	}
}
