package mfurmane.nearUserObjects;

import java.util.HashMap;
import java.util.Map;

public class Receipt {

	private Map<String, Double> pricePerProduct = new HashMap<>();
	private double discount = 0;
	private double totalPrice = 0;
	
	public Map<String, Double> getPricePerProduct() {
		return pricePerProduct;
	}
	public void setPricePerProduct(Map<String, Double> pricePerProduct) {
		this.pricePerProduct = pricePerProduct;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
}
