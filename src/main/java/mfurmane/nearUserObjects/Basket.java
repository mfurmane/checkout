package mfurmane.nearUserObjects;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import mfurmane.databaseAccess.CheaperConnection;
import mfurmane.databaseAccess.Entity;
import mfurmane.databaseAccess.EntityManagerInterface;

public class Basket implements BasketInterface {

	private Map<String, Long> basket = new HashMap<>();
	private Map<String, Entity> entityMapping = new HashMap<>();
	private EntityManagerInterface manager;

	@Autowired
	public Basket(EntityManagerInterface manager) {
		this.manager = manager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mfurmane.nearUserObjects.BasketInterface#addItems(java.lang.String,
	 * long)
	 */
	@Override
	public Entity addItems(String id, long count) {
		if (basket.containsKey(id))
			basket.put(id, basket.get(id) + count);
		else {
			Entity entity = manager.getEntityByIdentifier(id);
			if (entity == null)
				return null;
			entityMapping.put(id, entity);
			basket.put(id, count);
		}
		return entityMapping.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mfurmane.nearUserObjects.BasketInterface#clear()
	 */
	@Override
	public void clear() {
		basket.clear();
	}

	public Map<String, Long> getBasket() {
		return basket;
	}

	public double calculateValue(Receipt receipt) {
		double sum = 0;
		for (String id : basket.keySet()) {
			Entity entity = entityMapping.get(id);
			double presum = sum;
			if (entity.getExtraPriceCount() == 0) {
				sum += (entity.getPrice() * basket.get(id));
			} else {
				long count = basket.get(id) / entity.getExtraPriceCount();
				sum += (count * entity.getExtraPrice());
				sum += ((basket.get(id) - (count * entity.getExtraPriceCount())) * entity.getPrice());
			}
			receipt.getPricePerProduct().put(entity.getName(), sum - presum);
		}
		return sum;
	}

	public double calculateDiscount() {
		double sum = 0;
		for (CheaperConnection cheaperConnection : manager.getCheaperConnections()) {
			String firstIdentifier = cheaperConnection.getConnectionId().getFirstId().getIdentifier();
			String secondIdentifier = cheaperConnection.getConnectionId().getSecondId().getIdentifier();
			if (basket.keySet().contains(firstIdentifier) && basket.keySet().contains(secondIdentifier)) {
				sum += (Math.min(basket.get(firstIdentifier), basket.get(secondIdentifier))
						* cheaperConnection.getSave());
			}
		}
		return sum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mfurmane.nearUserObjects.BasketInterface#actualReceipt()
	 */
	@Override
	public Receipt actualReceipt() {
		Receipt receipt = new Receipt();
		receipt.setDiscount(calculateDiscount());
		receipt.setTotalPrice(calculateValue(receipt) - receipt.getDiscount());
		return receipt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mfurmane.nearUserObjects.BasketInterface#prepareData()
	 */
	@Override
	public void prepareData() {
		manager.prepareData();
	}

}
