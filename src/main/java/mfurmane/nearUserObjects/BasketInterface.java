package mfurmane.nearUserObjects;

import mfurmane.databaseAccess.Entity;

public interface BasketInterface {

	Entity addItems(String id, long count);

	void clear();

	Receipt actualReceipt();

	void prepareData();

}