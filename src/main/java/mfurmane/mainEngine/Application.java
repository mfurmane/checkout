package mfurmane.mainEngine;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.SessionScope;

import mfurmane.databaseAccess.CheaperConnectionDAO;
import mfurmane.databaseAccess.CheaperConnectionDAOInterface;
import mfurmane.databaseAccess.EntityDAO;
import mfurmane.databaseAccess.EntityDAOInterface;
import mfurmane.databaseAccess.EntityManager;
import mfurmane.databaseAccess.EntityManagerInterface;
import mfurmane.nearUserObjects.Basket;
import mfurmane.nearUserObjects.BasketInterface;

@SpringBootApplication
public class Application {

	@Bean
	public EntityManagerInterface entityManager() {
		return new EntityManager();
	}

	@Bean
	@SessionScope
	public BasketInterface basket() {
		return new Basket(entityManager());
	}

	@Bean
	public EntityDAOInterface entityAccess() {
		return new EntityDAO();
	}

	@Bean
	public CheaperConnectionDAOInterface connectionsAccess() {
		return new CheaperConnectionDAO();
	}

	@Bean
	public SessionFactory sessionFactory() {
		Configuration configuration = new Configuration();
		return configuration.configure().buildSessionFactory();
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
