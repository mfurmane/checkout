package mfurmane.mainEngine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mfurmane.databaseAccess.Entity;
import mfurmane.nearUserObjects.BasketInterface;
import mfurmane.nearUserObjects.Receipt;

@RestController
public class CheckoutController {

	private BasketInterface basket;// = new Basket();

	@Autowired
	public CheckoutController(BasketInterface basket) {
		this.basket = basket;
	}

	@RequestMapping("/add_to_basket")
	public String addToBasket(@RequestParam(value = "identifier", defaultValue = "AAAA") String identifier,
			@RequestParam(value = "count", defaultValue = "1") long count) {
		Entity added = basket.addItems(identifier, count);
		if (added != null)
			return "added " + count + " of " + identifier;
		else
			throw new RuntimeException("Item doesn't exist in database");
	}

	@RequestMapping("/get_actual_price")
	public Receipt getActualPrice() {
		return basket.actualReceipt();
	}

	@RequestMapping("/clear_basket")
	public void clearBasket() {
		basket.clear();
	}

	@RequestMapping("/prepare_data")
	public void prepareData() {
		basket.prepareData();
	}

}
