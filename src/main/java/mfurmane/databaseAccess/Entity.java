package mfurmane.databaseAccess;

public class Entity {
	
	private String identifier;
	private String name;
	private double price;
	private double extraPrice;
	private int extraPriceCount;

	public Entity() {
		
	}
	
	public Entity(String id, String name, double price, int extraPriceCount, double extraPrice) {
		this.name = name;
		this.identifier = id;
		this.price = price;
		this.extraPriceCount = extraPriceCount;
		this.extraPrice = extraPrice;
	}
	
	public double getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public double getExtraPrice() {
		return extraPrice;
	}

	public void setExtraPrice(double extraPrice) {
		this.extraPrice = extraPrice;
	}

	public int getExtraPriceCount() {
		return extraPriceCount;
	}

	public void setExtraPriceCount(int extraPriceCount) {
		this.extraPriceCount = extraPriceCount;
	}
	
	
	
}
