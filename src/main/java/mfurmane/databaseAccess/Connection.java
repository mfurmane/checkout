package mfurmane.databaseAccess;

import java.io.Serializable;

public class Connection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -855796075401490294L;
	private Entity firstId, secondId;
	public Connection() {
		
	}
	public Entity getFirstId() {
		return firstId;
	}
	public void setFirstId(Entity firstId) {
		this.firstId = firstId;
	}
	public Entity getSecondId() {
		return secondId;
	}
	public void setSecondId(Entity secondId) {
		this.secondId = secondId;
	}

	
	
	
}
