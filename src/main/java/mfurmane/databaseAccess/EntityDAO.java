package mfurmane.databaseAccess;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class EntityDAO implements EntityDAOInterface {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.EntityDAOInterface#insertEntity(java.lang.String, java.lang.String, double, int, double)
	 */
	@Override
	public Entity insertEntity(String id, String name, double price, int extraPriceCount, double extraPrice) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Entity entity = new Entity(id, name, price, extraPriceCount, extraPrice);
		session.save(entity);
		session.flush();
		session.close();
		return entity;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.EntityDAOInterface#getEntityByIdentifier(java.lang.String)
	 */
	@Override
	public Entity getEntityByIdentifier(String id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Entity entity = (Entity) session.get(Entity.class, id);

		session.close();

		return entity;
	}

}
