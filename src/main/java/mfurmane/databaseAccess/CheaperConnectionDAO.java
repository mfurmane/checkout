package mfurmane.databaseAccess;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

public class CheaperConnectionDAO implements CheaperConnectionDAOInterface {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.CheaperConnectionDAOInterface#insert_discount(java.lang.String, java.lang.String, double)
	 */
	@Override
	public CheaperConnection insertDiscount(String firstIdentifier, String secondIdentifier, double discount) {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		Query<Entity> query = session.createQuery("from Entity where identifier=:firstIdentifier");
		query.setParameter("firstIdentifier", firstIdentifier);
		Entity entity1 = (Entity) query.uniqueResult();
		session.close();
		session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		Query<Entity> query2 = session.createQuery("from Entity where identifier=:secondIdentifier");
		query2.setParameter("secondIdentifier", secondIdentifier);
		Entity entity2 = (Entity) query2.uniqueResult();

		CheaperConnection cheaperConnection = new CheaperConnection();
		Connection connection = new Connection();
		connection.setFirstId(entity1);
		connection.setSecondId(entity2);
		cheaperConnection.setConnectionId(connection);
		cheaperConnection.setSave(discount);
		session.beginTransaction();
		session.save(cheaperConnection);
		session.flush();
		session.close();

		return cheaperConnection;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.CheaperConnectionDAOInterface#getCheaperConnections()
	 */
	@Override
	public List<CheaperConnection> getCheaperConnections() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		Query<CheaperConnection> query = session.createQuery("from CheaperConnection");
		List<CheaperConnection> cheaperConnections = query.list();
		session.close();
		return cheaperConnections;
	}

}
