package mfurmane.databaseAccess;

public interface EntityDAOInterface {

	Entity insertEntity(String id, String name, double price, int extraPriceCount, double extraPrice);

	Entity getEntityByIdentifier(String id);

}