package mfurmane.databaseAccess;

import java.util.List;

public interface EntityManagerInterface {

	Entity getEntityByIdentifier(String identifier);

	void prepareData();

	List<CheaperConnection> getCheaperConnections();

}