package mfurmane.databaseAccess;

import java.util.List;

public interface CheaperConnectionDAOInterface {

	CheaperConnection insertDiscount(String firstIdentifier, String secondIdentifier, double discount);

	List<CheaperConnection> getCheaperConnections();

}