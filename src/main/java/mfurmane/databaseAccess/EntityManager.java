package mfurmane.databaseAccess;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class EntityManager implements EntityManagerInterface {

	private EntityDAOInterface edao;
	private CheaperConnectionDAOInterface cdao;

	@Autowired
	public void setDao(EntityDAOInterface edao, CheaperConnectionDAOInterface cdao) {
		this.edao = edao;
		this.cdao = cdao;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.EntityManagerInterface#getEntityByIdentifier(java.lang.String)
	 */
	@Override
	public Entity getEntityByIdentifier(String identifier) {
		Entity entity = edao.getEntityByIdentifier(identifier);
		return entity;
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.EntityManagerInterface#prepareData()
	 */
	@Override
	public void prepareData() {
		edao.insertEntity("AAAA", "A", 40, 3, 70);
		edao.insertEntity("BBBB", "B", 10, 2, 15);
		edao.insertEntity("CCCC", "C", 30, 4, 60);
		edao.insertEntity("DDDD", "D", 25, 2, 40);
		edao.insertEntity("EEEE", "E", 15, 12, 160);
		edao.insertEntity("FFFF", "F", 35, 3, 100);
		cdao.insertDiscount("AAAA", "BBBB", 5);
		cdao.insertDiscount("AAAA", "DDDD", 10);
	}

	/* (non-Javadoc)
	 * @see mfurmane.databaseAccess.EntityManagerInterface#getCheaperConnections()
	 */
	@Override
	public List<CheaperConnection> getCheaperConnections() {
		return cdao.getCheaperConnections();
	}

}
